package com.nesterov;

import java.util.Deque;
import java.util.LinkedList;

public class DequeExample {
    public static void main(String[] args) {
        // Создаем объект Deque.
        Deque<String> deque = new LinkedList<>();

        // Добавляем элементы в начало и конец очереди.
        deque.addFirst("First");
        deque.addLast("Last");

        // Удаление элементов с начала и конца очереди.
        String firstElement = deque.removeFirst();
        String lastElement = deque.removeLast();

        // Вывод элементов.
        System.out.println("First Element: " + firstElement);
        System.out.println("Last Element: " + lastElement);
    }
}
